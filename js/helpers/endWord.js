export const endWord = (count) => {
	count %= 100;
	if (count >= 5 && count <= 20) {
		return 'фильмов';
	}

	count %= 10;
	if (count === 1) {
		return 'фильм';
	}

	if (count >= 2 && count <= 4) {
		return 'фильма';
	}

	return 'фильмов';
}