import { saveRequest } from '../requests/requests.js';
import { renderTags, renderMovies, moveTagOnFirstPos } from '../render/render.js';
import { getFromLocalStorage, setToLocalStorage } from '../localStorage/localStorage.js';
import { debounce } from "../debounce/index.js";

export const submitInput = () => {
	document.querySelector('.page-search__input').addEventListener('keydown', function (e) {
		if (e.code === 'Enter') {
			const inputValue = this.value.toLowerCase();
			checkSearchCache(inputValue);
			this.value = "";
		}
	});
}

const checkSearchCache = (inputValue) => {
	if (getFromLocalStorage('tinkoffProkatTags').includes(inputValue)) {
		moveTagOnFirstPos(inputValue);
	} else {
		saveRequest(inputValue)
			.then(() => { renderTags() })
			.then(() => { renderMovies() })
			.then(() => { addListenerOnTags() })
	}
};

const debounceSearch = debounce(checkSearchCache, 1000);

export const onChangeInput = () => {
	document.querySelector('.page-search__input').addEventListener('input', function (e) {
		const inputValue = this.value.toLowerCase();
		debounceSearch(inputValue);
	})
};

export const resetInput = () => {
	document.querySelector('.page-search__cancle').addEventListener('click', function (e) {
		document.querySelector('.page-search__input').value = "";
	})
}

const singleClickOnTag = (e) => {
	moveTagOnFirstPos(e.textContent)
}

const doubleClickOnTag = (e) => {

	let tags = getFromLocalStorage('tinkoffProkatTags');
	const posOfCurrentTag = tags.indexOf(e.textContent);
	const searchCache = getFromLocalStorage('tinkoffProkatSearchCache');;

	tags = [...tags.slice(0, posOfCurrentTag), ...tags.slice(posOfCurrentTag + 1)]
	setToLocalStorage('tinkoffProkatTags', tags);

	delete searchCache[e.textContent];
	setToLocalStorage('tinkoffProkatSearchCache', searchCache);

	if (posOfCurrentTag === 0) {
		setToLocalStorage('tinkoffProkatMovies', "");
	}

	renderTags();
	addListenerOnTags();
	renderMovies();
}

export const addListenerOnTags = () => {
	const tags = Array.from(document.querySelectorAll('.page-search__tag'));
	let clickCount = 0;
	let singleClickTimer;

	tags.forEach((element) => {
		element.addEventListener('click', function () {
			clickCount++;
			if (clickCount === 1) {
				singleClickTimer = setTimeout(() => {
					clickCount = 0;
					singleClickOnTag(this);
				}, 300)
			} else if (clickCount === 2) {
				clearTimeout(singleClickTimer);
				clickCount = 0;
				doubleClickOnTag(this);
			}
		}, false)
	})
};


export const initialEvents = () => {
	addListenerOnTags();
	submitInput();
	onChangeInput();
	resetInput();
}



