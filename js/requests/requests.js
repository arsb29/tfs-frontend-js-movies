import { getFromLocalStorage, setToLocalStorage } from '../localStorage/localStorage.js';

export const makeRequests = async (input) => {
	const pageResult = document.querySelector('.page-results');
	pageResult.classList.add('_sending')
	try {
		let totalResults;
		let filmsUrl;
		let request = await fetch(`http://www.omdbapi.com/?type=movie&apikey=7ea4aa35&s=${input}`)
			.then((r) => r.json())
			.then((r) => {
				totalResults = r.totalResults;
				filmsUrl = r.Search.map((e) => { return `http://www.omdbapi.com/?apikey=7ea4aa35&i=${e.imdbID}` });
				return r
			})

		let requests = filmsUrl.map(url => fetch(url));

		let fullData = await Promise.all(requests)
			.then(responses => Promise.all(responses.map(r => r.json())))
			// все JSON-ответы обработаны, users - массив с результатами
			.then(films => films.map((film) => {
				return {
					Title: film.Title,
					Poster: film.Poster,
					Genre: film.Genre,
					Year: film.Year,
					Ratings: (parseFloat(film.Ratings[0].Value.slice(0, 3)) / 2).toFixed(1)
				}
			}));
		pageResult.classList.remove('_sending');
		return {
			count: totalResults,
			results: fullData
		}

	}
	catch (error) {
		pageResult.classList.remove('_sending');
		return { error };
	}

}

export const saveRequest = async (newRequest = "") => {
	if (newRequest !== '') {
		let searchCache = getFromLocalStorage('tinkoffProkatSearchCache');
		let tags = getFromLocalStorage('tinkoffProkatTags');

		const newRequestResult = await makeRequests(newRequest);

		searchCache[newRequest] = JSON.stringify(newRequestResult);
		setToLocalStorage('tinkoffProkatSearchCache', searchCache)

		tags.unshift(newRequest);
		setToLocalStorage('tinkoffProkatTags', tags)

		setToLocalStorage('tinkoffProkatMovies', newRequestResult)
	}
}