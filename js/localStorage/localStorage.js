export const getFromLocalStorage = (key) => {
	return JSON.parse(localStorage.getItem(key));
}

export const setToLocalStorage = (key, item) => {
	localStorage.setItem(key, JSON.stringify(item))
}

export const initialLocalStorage = () => {
	const searchCache = getFromLocalStorage('tinkoffProkatSearchCache');
	const tags = getFromLocalStorage('tinkoffProkatTags');
	const movies = getFromLocalStorage('tinkoffProkatMovies');
	if (searchCache === null) {
		setToLocalStorage('tinkoffProkatSearchCache', {})
	}
	if (tags === null) {
		setToLocalStorage('tinkoffProkatTags', [])
	}
	if (movies === null) {
		setToLocalStorage('tinkoffProkatMovies', '')
	}
}
