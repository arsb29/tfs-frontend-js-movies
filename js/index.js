import { initialRender } from './render/render.js';
import { initialEvents } from './events/events.js';
import { initialLocalStorage } from './localStorage/localStorage.js';
// import { saveRequest } from './requests/requests.js';

// let keys = Object.keys(localStorage);
// for (let key of keys) {
// 	localStorage.removeItem(key);
// }


const initial = () => {
	initialLocalStorage();
	initialEvents();
	initialRender();
}

initial();




