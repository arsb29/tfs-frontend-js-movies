import { getFromLocalStorage, setToLocalStorage } from '../localStorage/localStorage.js';
import { addListenerOnTags } from '../events/events.js';
import { endWord } from '../helpers/endWord.js';


export const renderTags = () => {
	const tags = document.querySelector('.page-search__tags');
	const keys = getFromLocalStorage('tinkoffProkatTags');

	tags.innerHTML = '';
	keys.forEach((element) => {
		tags.innerHTML += `<div class="page-search__tag btn">${element}</div>`;
	});
}

export const moveTagOnFirstPos = (tag) => {
	let tags = JSON.parse(localStorage.getItem(`tinkoffProkatTags`));
	const posOfCurrentTag = tags.indexOf(tag);
	const resOfCurrentTag = JSON.parse(getFromLocalStorage('tinkoffProkatSearchCache')[tag]);

	tags = [tag, ...tags.slice(0, posOfCurrentTag), ...tags.slice(posOfCurrentTag + 1)]

	setToLocalStorage('tinkoffProkatTags', tags);
	setToLocalStorage('tinkoffProkatMovies', resOfCurrentTag);

	renderTags();
	addListenerOnTags();
	renderMovies();
}

export const renderMovies = () => {
	const movies = JSON.parse(localStorage.getItem(`tinkoffProkatMovies`));
	const title = document.querySelector('.page-results__title');
	const items = document.querySelector('.page-results__items');
	items.innerHTML = '';

	if (movies === '') {
		title.innerHTML = ``;
	} else if ('error' in movies) {
		title.innerHTML = `<h2 class="title">Мы не поняли о чем речь ¯\_(ツ)_/¯</h2>`;

	} else {
		const { count, results } = movies;

		title.innerHTML = `<h2 class="title">Нашли ${count} ${endWord(count)}</h2>`;

		results.forEach(element => {
			items.innerHTML += `
		<div class="page-results__item">
		<div class="item-result">
			${element.Poster === 'N/A' ? "" : '<img src=' + element.Poster + 'alt="" class="item-result__image">'} 
			<div class="item-result__cover">
				<div class="item-result__info">
					<div class="item-result__feedback">
						<div class="item-result__emoji">
							<img src="img/r0${Math.ceil(element.Ratings)}.png" alt="" />
						</div>
						<div class="item-result__raiting">${element.Ratings}</div>
					</div>
					<h3 class="item-result__title">${element.Title}</h3>
					<div class="item-result__undertitle">
						<span class="item-result__category">${element.Genre}</span>
						<span class="item-result__year">${element.Year}</span>
					</div>
				</div>
			</div>
		</div>
	</div>`;
		})

	}
}

export const initialRender = () => {
	renderTags();
	renderMovies();
}